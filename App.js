import React, { Component } from 'react';
import PushNotification from "react-native-push-notification";
import firebase from 'react-native-firebase';
import { View, Text } from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  

// async componentWillMount() {
//   await executeFirebase();
// }

  async componentDidMount() {
    PushNotification.configure({

      onNotification: function(notification) {
        console.log("NOTIFICATION:", notification);
      }
    });
  }

  async executeFirebase() {
    console.log(firebase)
    const token = firebase.messaging().getToken();
    token && token.then(res => console.log(res))
  }

  render() {
    this.executeFirebase()
    return (
      <View style={{ justifyContent:"center", alignItems:"center" }}>
        <Text> Recieved Push Notification </Text>
      </View>
    );
  }
}
